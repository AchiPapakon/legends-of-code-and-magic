interface PotentialAttacks {
    myCard: Card;
    enemyCard: Card | -1;
    priority: number;
}

interface Draft {
    index: number;
    priority: number;
}

enum CardType {
    mob,
    greenItem,
    redItem,
    blueItem
}

enum CardLocation {
    hand = 0,
    playerBoard = 1,
    enemyBoard = -1
}

class Card {
    private _hasAttacked: boolean = false;
    private _toBeSummoned: boolean = false;
    public ability: Ability;
    public draft: Draft = {
        index: null,
        priority: null
    };
    public canPotentiallyAttack: boolean = true;
    constructor (
        public cardNumber: number,
        public instanceId: number, 
        public location: CardLocation,
        public cardType: CardType, 
        public cost: number,
        public attack: number,
        public defense: number,
        public abilities: string,
        public myHealthChange: number,
        public enemyHealthChange: number,
        public cardDraw: number) {
            this.ability = new Ability(abilities);
        }
    get hasAttacked(): boolean {
        return this._hasAttacked;
    }

    set hasAttacked(_hasAttacked) {
        this._hasAttacked = _hasAttacked;
    }

    get canAttack(): boolean {
        return !this.hasAttacked && this.attack > 0;
    }

    get canBeSummoned(): boolean {
        return !this._toBeSummoned;
    }

    summon(): void {
        this._toBeSummoned = true;
        if (this.ability.C) {
            this.location = CardLocation.playerBoard; // TODO: Bad coding to imply that this card can attack in the same round that it's summoned
        }
    }

    disable(): void {
        this._toBeSummoned = true;
    }
}

class Ability {
    public B: boolean = null;
    public C: boolean = null;
    public G: boolean = null;
    public D: boolean = null;
    public L: boolean = null;
    public W: boolean = null;
    constructor(private abilitiesString: string) {
        this.B = abilitiesString.includes('B');
        this.C = abilitiesString.includes('C');
        this.G = abilitiesString.includes('G');
        this.D = abilitiesString.includes('D');
        this.L = abilitiesString.includes('L');
        this.W = abilitiesString.includes('W');
    }
}

class Player {
    constructor (
        public health: number,
        public mana: number,
        public deck: number,
        public rune: number,
        public draw: number
    ) {}

    summon(card: Card): void {
        card.summon();
        this.mana -= card.cost;
    }
}

class Actions {
    private static _actions: string[] = [];
    private constructor() {} // Cannot create an instance using 'new'

    static clear(): void {
        Actions._actions = [];
    }

    static getActionSequence(): string {
        return Actions._actions.join(';');
    }

    static push(__input: string): void {
        Actions._actions.push(__input);
    }

    static summonCards(player: Player, cards: Card[]): void {
        let cardsOnMyBoard = cards.reduce((acc, card) => {
            return card.location === CardLocation.playerBoard ? ++acc : acc;
        }, 0);

        // If the board is full, then skip the summoning altogether.
        if (cardsOnMyBoard >= 6) {
            return;
        }

        const cardsOnHand = cards.reduce((acc, card) => {
            return card.location === CardLocation.hand ? ++acc : acc;
        }, 0);

        // TODO: Check if I can clear the board and still have a mob there.

        const handCards = cards.filter(card => card.location === CardLocation.hand);
        const mobCards = handCards.filter(card => card.cardType === CardType.mob);
        const tankCards = mobCards.filter(card => card.ability.G);

        this.itemAttackSimulate(player, cards, CardType.greenItem);

        tankCards.sort((a, b) => b.cost - a.cost);
        tankCards.filter(card => card.canBeSummoned).some(card => {
            // The method 'some' stops in the first occurrence if we return true.
            if (cardsOnMyBoard < 6 && player.mana >= card.cost) {
                Actions.pushSummon(player, card);
                cardsOnMyBoard++;
                return player.mana === 0;
            }
        });
        mobCards.sort((a, b) => b.cost - a.cost);
        mobCards.filter(card => card.canBeSummoned).some(card => {
            // The method 'some' stops in the first occurrence if we return true.
            if (cardsOnMyBoard < 6 && player.mana >= card.cost) {
                Actions.pushSummon(player, card);
                cardsOnMyBoard++;
                return player.mana === 0;
            }
        });
        // If the mana is enough, check if we can use items.
        this.itemAttackSimulate(player, cards);
    }

    static pushSummon(player: Player, card: Card) {
        Actions.push(`SUMMON ${card.instanceId}`);
        player.summon(card);
    }

    static doAttack(myCard: Card, opponentCard: Card | -1): void {
        if (opponentCard === -1) {
            Actions.push(`ATTACK ${myCard.instanceId} -1`);
        } else {
            Actions.push(`ATTACK ${myCard.instanceId} ${opponentCard.instanceId}`);
            // If the enemy has a shield (=W), then lose it
            if (opponentCard.ability.W) {
                opponentCard.ability.W = false;
            }
            if (myCard.ability.L && !opponentCard.ability.W) {
                opponentCard.defense = 0
            } else {
                opponentCard.defense -= myCard.attack;
            }
        }
        myCard.hasAttacked = true;
    }

    static doUseItem(player: Player, myItem: Card, enemyCard: Card | -1): void {
        if (enemyCard === -1) {
            Actions.push(`USE ${myItem.instanceId} -1`);
        } else {
            Actions.push(`USE ${myItem.instanceId} ${enemyCard.instanceId}`);
            Actions.removeAbilities(myItem, enemyCard);
            enemyCard.defense += myItem.defense;
            enemyCard.attack += myItem.attack;
            if (myItem.defense < 0 && enemyCard.ability.W) {
                enemyCard.ability.W = false;
            }
        }

        myItem.disable();
        player.mana -= myItem.cost;
    }

    static removeAbilities(myItem: Card, enemyCard: Card) {
        if (myItem.ability.B && enemyCard.ability.B) {
            enemyCard.ability.B = false;
        }

        if (myItem.ability.C && enemyCard.ability.C) {
            enemyCard.ability.C = false;
        }

        if (myItem.ability.D && enemyCard.ability.D) {
            enemyCard.ability.D = false;
        }

        if (myItem.ability.G && enemyCard.ability.G) {
            enemyCard.ability.G = false;
        }

        if (myItem.ability.L && enemyCard.ability.L) {
            enemyCard.ability.L = false;
        }

        if (myItem.ability.W && enemyCard.ability.W) {
            enemyCard.ability.W = false;
        }
    }

    static itemAttackSimulate(player: Player, cards: Card[], cardType?: CardType) {
        const enemyCards = cards.filter(card => card.location === CardLocation.enemyBoard);
        const handCards = cards.filter(card => card.location === CardLocation.hand);
        // const redBlueItemCards = handCards.filter(card => card.cardType === 2 || card.cardType === 3);
        const myItems = handCards.filter(card => card.cardType > 0);
        if (enemyCards.length === 0) {
            return;
        }
        while (myItems.filter(myItem => player.mana >= myItem.cost && myItem.canBeSummoned).length > 0) {
            let potentialAttacks: PotentialAttacks[] = [];
            const myMobsOnBoard = cards.filter(card => card.location === CardLocation.playerBoard);
            const summonableItems = myItems.filter(myItem => player.mana >= myItem.cost && myItem.canBeSummoned);
            summonableItems.forEach(myItem => {
                const aliveEnemyMobs = enemyCards.filter(enemyCard => enemyCard.defense > 0);
                let eligibleForGreenItem: boolean = false;
                let priority = 0;

                /**
                 * First, try to use the green items
                 */

                /**
                 * Give attack bonus to defensive mobs
                 */
                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                eligibleForGreenItem = myItem.cardType === CardType.greenItem && myMobsOnBoard.length > 0;
                if (eligibleForGreenItem && myItem.attack >= myItem.defense) {
                    myMobsOnBoard.sort((a, b) => b.defense - a.defense);
                    if (myMobsOnBoard[0].defense > 1) {
                        potentialAttacks.push({myCard: myItem, enemyCard: myMobsOnBoard[0], priority});
                        myItem.canPotentiallyAttack = false;
                    }
                }

                /**
                 * Give defense bonus to aggressive mobs
                 */
                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                if (eligibleForGreenItem && myItem.attack < myItem.defense) {
                    myMobsOnBoard.sort((a, b) => b.attack - a.attack);
                    if (myMobsOnBoard[0].attack > 1) {
                        potentialAttacks.push({myCard: myItem, enemyCard: myMobsOnBoard[0], priority});
                        myItem.canPotentiallyAttack = false;
                    }
                }

                // Prevent using the rest of the 'while' logic with a green card
                if (myItem.cardType === CardType.greenItem) {
                    return;
                }

                /**
                 * Then, try to use the blue and red items
                 */
                if (cardType && cardType === CardType.greenItem) {
                    return;
                }
                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.W && myItem.abilities === `BCDGLW` && myItem.defense < 0 && myItem.defense + enemyCard.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.L && myItem.abilities === `BCDGLW` && myItem.defense < 0 && myItem.defense + enemyCard.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.D && myItem.abilities === `BCDGLW` && myItem.defense < 0 && myItem.defense + enemyCard.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.G && myItem.abilities === `BCDGLW` && myItem.defense < 0 && myItem.defense + enemyCard.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.abilities !== '------' && myItem.abilities === `BCDGLW` && myItem.defense < 0 && enemyCard.defense + myItem.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.W && myItem.defense === -1) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (!enemyCard.ability.W && myItem.defense < 0 && enemyCard.defense + myItem.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.G && myItem.ability.G) { // TODO: Hardcode all the items that remove Guard.
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.abilities !== '------' && myItem.abilities === 'BCDGLW') {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (myItem.defense < 0 && enemyCard.defense + myItem.defense <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                        return;
                    }
                    if (myItem.attack < 0 && enemyCard.attack + myItem.attack <= 0) {
                        potentialAttacks.push({myCard: myItem, enemyCard, priority});
                        myItem.canPotentiallyAttack = false;
                    }
                });

                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                priority++;
                if (myItem.attack === 0 && myItem.defense === 0) {
                    potentialAttacks.push({myCard: myItem, enemyCard: -1, priority});
                    myItem.canPotentiallyAttack = false;
                }

                // If we want to keep the item for later use, then we have to prevent an infinite loop by disabling it for this turn.
                if (!myItem.canBeSummoned || !myItem.canPotentiallyAttack) {
                    return;
                }
                // priority++;
            });

            if (potentialAttacks.length === 0) {
                return;
            }

            potentialAttacks.sort((a, b) => a.priority - b.priority);
            console.error('Items', summonableItems.length);
            console.error(potentialAttacks);
            potentialAttacks.forEach(potentialAttack => {
                if (potentialAttack.enemyCard === -1) {
                    console.error(`Item [${potentialAttack.priority}] ${potentialAttack.myCard.instanceId} -> -1`);
                } else {
                    console.error(`Item [${potentialAttack.priority}] ${potentialAttack.myCard.instanceId} -> ${potentialAttack.enemyCard.instanceId} ${potentialAttack.enemyCard.abilities}`);
                }
            })
            Actions.doUseItem(player, potentialAttacks[0].myCard, potentialAttacks[0].enemyCard);
            myItems.forEach(card => card.canPotentiallyAttack = true);
        }
    }

    static mobAttackSimulate(myCards: Card[], enemyCards: Card[]) {
        if (enemyCards.length === 0) {
            return;
        }
        while (myCards.filter(myCard => myCard.canAttack).length > 0) {
            let potentialAttacks: PotentialAttacks[] = [];
            const myCardsThatCanAttack = myCards.filter(myCard => myCard.canAttack);
            myCardsThatCanAttack.forEach(myCard => {
                const aliveEnemyMobs = enemyCards.filter(enemyCard => enemyCard.defense > 0);
                let priority = 0;

                /**
                 * Destroy enemy's shield with a 1 attack mob
                 */
                priority++;
                if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                    return;
                }
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                        return;
                    }
                    if (enemyCard.ability.W && myCard.attack === 1) {
                        potentialAttacks.push({myCard, enemyCard, priority});
                        myCard.canPotentiallyAttack = false;
                    }
                });
                
                /**
                 * Venom Hedgehog (1/1 L)
                 * Find the enemy with the highest Attack + Defense.
                 */
                priority++;
                if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                    return;
                }
                if (myCard.cardNumber === 48 && aliveEnemyMobs.length > 0) {
                    let bestTarget: Card;
                    aliveEnemyMobs.forEach(enemyCard => {
                        if (enemyCard.ability.W) {
                            return;
                        }
                        if (!bestTarget || enemyCard.defense + enemyCard.attack > bestTarget.defense + bestTarget.attack) {
                            bestTarget = enemyCard;
                        }
                    });
                    if (bestTarget) {
                        potentialAttacks.push({myCard, enemyCard: bestTarget, priority});
                        myCard.canPotentiallyAttack = false;
                    }
                }
                
                /**
                 * Lethal mob.
                 * Find the enemy with the highest Attack + Defense. // TODO: Improve this logic.
                 */
                priority++;
                if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                    return;
                }
                if (myCard.ability.L && aliveEnemyMobs.length > 0) {
                    let bestTarget: Card;
                    aliveEnemyMobs.forEach(enemyCard => {
                        if (enemyCard.ability.W) {
                            return;
                        }
                        if (!bestTarget || enemyCard.defense + enemyCard.attack > bestTarget.defense + bestTarget.attack) {
                            bestTarget = enemyCard;
                        }
                    });
                    if (bestTarget) {
                        potentialAttacks.push({myCard, enemyCard: bestTarget, priority});
                        myCard.canPotentiallyAttack = false;
                    }
                }
        
                /**
                 * Kill enemy mob with 0 attack loss while my mob survives.
                 */
                // priority++;
                // if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                //     return;
                // }
                // aliveEnemyMobs.forEach(enemyCard => {
                //     if (!myCard.canAttack || !myCard.canPotentiallyAttack || enemyCard.ability.W || enemyCard.ability.L) {
                //         return;
                //     }
                //     if (myCard.defense > enemyCard.attack && myCard.attack === enemyCard.defense) {
                //         potentialAttacks.push({myCard, enemyCard, priority});
                //         myCard.canPotentiallyAttack = false;
                //     }
                // });
                
                /**
                 * Kill enemy mob with attack loss while my mob survives.
                 */
                // priority++;
                // if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                //     return;
                // }
                // aliveEnemyMobs.forEach(enemyCard => {
                //     if (!myCard.canAttack || !myCard.canPotentiallyAttack || enemyCard.ability.W || enemyCard.ability.L) {
                //         return;
                //     }
                //     if (myCard.defense > enemyCard.attack && myCard.attack > enemyCard.defense) {
                //         potentialAttacks.push({myCard, enemyCard, priority});
                //         myCard.canPotentiallyAttack = false;
                //     }
                // });
        
                /**
                 * Kill enemy mob without attack loss while my mob dies.
                 */
                // priority++;
                // if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                //     return;
                // }
                // aliveEnemyMobs.forEach(enemyCard => {
                //     if (!myCard.canAttack || !myCard.canPotentiallyAttack || enemyCard.ability.W) {
                //         return;
                //     }
                //     if (myCard.attack === enemyCard.defense) {
                //         potentialAttacks.push({myCard, enemyCard, priority});
                //         myCard.canPotentiallyAttack = false;
                //     }
                // });
        
                /**
                 * Kill enemy mob with attack loss, while my mob survives,
                 * but my attack loss is less than the enemy's.
                 * => Adding a +1 modifier.
                 * Introducing a mini-priority which is the difference of the attack loss of mine versus the enemy's,
                 *     and we're taking it as 1/miniPriority.
                 */
                priority++;
                if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                    return;
                }
                let hasPotential: boolean = false;
                aliveEnemyMobs.forEach(enemyCard => {
                    if (!myCard.canAttack || !myCard.canPotentiallyAttack || enemyCard.ability.W) {
                        return;
                    }
                    if (myCard.attack >= enemyCard.defense) {
                        // if (myCard.attack - enemyCard.defense < enemyCard.attack - myCard.defense) {
                            const attackLoss = (myCard.attack - enemyCard.defense) - (enemyCard.attack - myCard.defense);
                            // We're justing adding 50 which is like a high number.
                            const miniPriority: number = (50 + attackLoss) / 100;
                            potentialAttacks.push({myCard, enemyCard, priority: priority + miniPriority});
                            hasPotential = true;
                        // }
                    }
                });
                if (hasPotential) {
                    myCard.canPotentiallyAttack = false;
                }

                /**
                 * If there is an enemy tank alive, attack it, || TODO: Improve this!!
                 * otherwise attack the enemy player.
                 */
                priority++;
                if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                    return;
                }
                hasPotential = false;
                if (myCard.canAttack || !myCard.canPotentiallyAttack) {
                    const aliveEnemyTanks = aliveEnemyMobs.filter(mob => mob.ability.G);
                    if (aliveEnemyTanks.length > 0) {
                        aliveEnemyTanks.forEach(enemyCard => {
                            if (!myCard.canAttack || !myCard.canPotentiallyAttack) {
                                return;
                            }
                            if (myCard.attack >= enemyCard.defense) {
                                const attackLoss = (myCard.attack - enemyCard.defense) - (enemyCard.attack - myCard.defense);
                                // We're justing adding 50 which is like a high number.
                                const miniPriority: number = (50 + attackLoss) / 100;
                                potentialAttacks.push({myCard, enemyCard, priority: priority + miniPriority});
                                hasPotential = true;
                            }
                        });
                        if (hasPotential) {
                            myCard.canPotentiallyAttack = false;
                        } else {
                            potentialAttacks.push({myCard, enemyCard: aliveEnemyTanks[0], priority: priority + 0.999});
                        }
                    } else {
                        potentialAttacks.push({myCard, enemyCard: -1, priority});
                    }
                    myCard.canPotentiallyAttack = false;
                }
            });
            potentialAttacks.sort((a, b) => a.priority - b.priority);
            console.error(`Mobs ${myCardsThatCanAttack.length}`);
            potentialAttacks.forEach(potentialAttack => {
                if (potentialAttack.enemyCard === -1) {
                    console.error(`Mob [${potentialAttack.priority}] ${potentialAttack.myCard.instanceId} -> -1`);
                } else {
                    console.error(`Mob [${potentialAttack.priority}] ${potentialAttack.myCard.instanceId} -> ${potentialAttack.enemyCard.instanceId} ${potentialAttack.enemyCard.abilities}`);
                }
            })
            Actions.doAttack(potentialAttacks[0].myCard, potentialAttacks[0].enemyCard);
            myCards.forEach(card => card.canPotentiallyAttack = true);
        }
    }
}

class Deck {
    private static _cards: Card[] = [];
    private static _sevens: number = 0;
    private static _cardsCost = {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0,
        10: 0,
        11: 0,
        12: 0
    };
    private static _itemsCount: number = 0;

    public static get cardsCost() {
        return this._cardsCost;
    }

    public static get itemsCount() {
        return this._itemsCount;
    }

    private constructor() {} // Cannot create an instance using 'new'
    static add(card: Card):void {
        // Cost
        if (card.cost > 6) {
            this._sevens++;
        }
        this._cardsCost[card.cost]++;

        // Item
        if (card.cardType > 0) {
            this._itemsCount++;
        }

        Deck._cards.push(card);
        console.log(`PICK ${card.draft.index}`);
    }
    static get cards(): Card[] {
        return this._cards;
    }
    static get atLeastThreeSevens() {
        return this._sevens > 2;
    }
}

// game loop
let round: number = 0;
while (true) {
    let me: Player;
    let enemy: Player;
    for (let i = 0; i < 2; i++) {
        // @ts-ignore
        let inputs: string[] = readline().split(' ');
        let [health, mana, deck, rune, draw] = inputs;
        if (i === 0) {
            console.error('Round:', ++round);
            console.error('Healt\tMana\tDeck\tRune\tDraw');
            me = new Player(Number(health), Number(mana), Number(deck), Number(rune), Number(draw))
        } else if (i === 1) {
            enemy = new Player(Number(health), Number(mana), Number(deck), Number(rune), Number(draw));
        }
        console.error(inputs.join('\t'));
    }
        // @ts-ignore
    var inputs: string[] = readline().split(' ');
    let [enemyHand, enemyActions] = inputs;
    console.error('Enemy hand:', enemyHand);
    for (let i = 0; i < Number(enemyActions); i++) {
        // @ts-ignore
        const cardNumberAndAction: string = readline();
        console.error('Enemy card number and action:', cardNumberAndAction);
    }
        // @ts-ignore
    const cardCount: number = parseInt(readline());
    let thisRoundCards: Card[] = [];
    for (let i = 0; i < cardCount; i++) {
        // @ts-ignore
        var inputs: string[] = readline().split(' ');
        let [cardNumber, instanceId, location, cardType, cost, attack,
            defense, abilities, myHealthChange, enemyHealthChange, cardDraw] = inputs;
        const newCard = new Card(Number(cardNumber), Number(instanceId), Number(location), Number(cardType),
                                Number(cost), Number(attack), Number(defense), abilities,
                                Number(myHealthChange), Number(enemyHealthChange), Number(cardDraw));
        thisRoundCards.push(newCard);
        if (i === 0) {
            console.error(`C No\ti ID\tLoc\tType\tCost\tAtk\tDef\tAbil\tmyHCh\topHCh\tcardDraw`)
        }
        console.error(inputs.join('\t'))
    }

    if (round < 31) {
        /**
         * ~~~~~~~~~~~~~~~~~~ PICKUP PHASE ~~~~~~~~~~~~~~~~~~
         */
        thisRoundCards = thisRoundCards.map((card, index) => {
            const isMob = card.cardType === CardType.mob;
            const isTankMob = isMob && card.ability.G;
            const isNonTankMob = isMob && !card.ability.G;
            const isItem = card.cardType > 0;
            const isGreenItem = card.cardType === CardType.greenItem;
            const isRedItem = card.cardType === CardType.redItem;
            const isBlueItem = card.cardType === CardType.blueItem;
            let priority: number;
            if (isTankMob) {
                priority = 0; // 0
            } else if (isNonTankMob) {
                priority = 0; // 1
            } else if (isRedItem || isBlueItem) {
                priority = 0; // 50
            } else if (isGreenItem) {
                priority = 0;
            } else {
                priority = null;
            }

            // Have a maximum of X cards in a specific mana cost slot without suffering a priority penalty
            if (isMob && card.cost === 0 && Deck.cardsCost[0] > 1) {
                priority += Deck.cardsCost[0] - 1;
            } else if (isMob && card.cost === 1 && Deck.cardsCost[1] > 1) {
                priority += Deck.cardsCost[1] - 1;
            } else if (Deck.cardsCost[card.cost] > 3) {
                priority += Deck.cardsCost[card.cost] - 3;
            }

            // Have maximum 3 Sevens without suffering a priority penalty.
            if (card.cost > 6 && Deck.atLeastThreeSevens) {
                priority += Deck.cardsCost[8] + Deck.cardsCost[9] + Deck.cardsCost[10] + Deck.cardsCost[11] + Deck.cardsCost[12] - 2;
            }

            // Specific cards
            const cNo: number = card.cardNumber;
            const goodCardPriorityChange: number = 1;
            /**
             * 48 Venom Hedgehog 1/1/1/L
             * 99 Cave Crab 3/2/5/G
             */
            if ([48, 99].includes(cNo)) {
                priority -= goodCardPriorityChange;
            }
            const superCrappyCardPriorityChange: number = 2;
            /**
             * 55 Hermit Slime 2/0/5/G
             * 92 Wurm 1/0/1/G/Gain 2 health
             * 101 Engulfer 4/3/4/G (Mana/Attack/Defense/Guard)
             * 107 Worker Shellcrab 5/3/3/G
             * 112 Rootkin Leader 6/4/7/G
             * 113 Tamed Bilespitter 6/2/4/Gain 4 health
             * 140 Grow Wings 2/0/0/C
             * 152 Mighty Throwing Axe 7/0/-7/Draw a card
             * 154 Poison - Deal 2 damage to the opponent. Draw a card.
             */
            if ([55, 92, 101, 107, 112, 113, 140, 152, 154].includes(cNo)) {
                priority += superCrappyCardPriorityChange;
            }
            const crappyCardPriorityChange: number = 1;
            /**
             * 24 Exploding Kitterbug 1/1/1/Deal 1 damage to  your opponent
             * 28 Infested Toad 2/1/2/Draw a card
             * 35 Snail-eyed Hulker 6/5/2/B
             * 110 Gargoyle 5/0/9/G
             */
            if ([24, 28, 35, 110].includes(cNo)) {
                priority += crappyCardPriorityChange;
            }

            // Have a maximum of 4 item cards without suffering a priority penalty
            if (isItem) {
                if (Deck.itemsCount > 3) {
                    priority += Deck.itemsCount - 3;
                } else if (round > 22) {
                    priority--;
                }
            }


            card.draft = {
                index,
                priority
            }
            if (card.draft.priority === null) {
                console.error(`/!\ Unhandled case!!! : draft.priority = null`);
            }
            return card;
        });
        console.error(thisRoundCards[0].draft.priority, thisRoundCards[1].draft.priority, thisRoundCards[2].draft.priority);
        thisRoundCards.sort((a, b) => a.draft.priority - b.draft.priority);
        Deck.add(thisRoundCards[0]);
        continue;
    }

    /**
     * ~~~~~~~~~~~~~~~~~~ SUMMON PHASE ~~~~~~~~~~~~~~~~~~
     */
    Actions.clear();
    Actions.summonCards(me, thisRoundCards);

    /**
     * ~~~~~~~~~~~~~~~~~~ ATTACK PHASE ~~~~~~~~~~~~~~~~~~
     */

    // List all of the enemy cards on board
    const enemyCards = thisRoundCards.filter((card) => card.location === CardLocation.enemyBoard);

    // List all of my cards on board
    const myCards = thisRoundCards.filter((card) => card.location === CardLocation.playerBoard);

    // List all of the enemy cards on board which are tanks
    const enemyTankCards = enemyCards.filter((card) => card.ability.G);
    const myTankMobs = myCards.filter(myCard => myCard.ability.G);
    const myNonTankMobs = myCards.filter(myCard => !myCard.ability.G);
    Actions.mobAttackSimulate(myCards, enemyTankCards);

    /**
     * Attack enemy non-tank cards
     */
    Actions.mobAttackSimulate(myNonTankMobs, enemyCards);

    /**
     * If I have tank mobs and the enemy doesn't have tank ones, then attack the player's face.
     */
    myTankMobs.filter(mob => mob.canAttack).forEach(tankMob => Actions.doAttack(tankMob, -1));
    Actions.summonCards(me, thisRoundCards);

    console.log(Actions.getActionSequence());
}
